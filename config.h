/*
Configuration file for DWM.
Maintainer: Vasco Nunes
*/

#include "push.c"
#include "moveresize.c"
#include "bstack.c"
#include "movestack.c"

/* Appearance *//*{{{*/

#define NUMCOLORS 5

//static const char font[] = "ohsnap.icons bold 13";
static const char font[] = "tamsynmod bold 12";

#define NUMCOLORS 20
static const char colors[NUMCOLORS][ColLast][20] = {
  // border foreground background         dec hex
  { "#001833", "#333840", "#000000" }, // 01  1 norm [dark grey on black]
  { "#1A6666", "#ACB4BF", "#000000" }, // 02  2 sel  [bright grey on black]
  { "#B641CC", "#E6E6E6", "#DC2F32" }, // 03  3 urg  [white on red]
  { "#001833", "#8C8B89", "#000000" }, // 04  4 mid grey on black
  { "#001833", "#001833", "#000000" }, // 05  5 dark blue on black
  { "#001833", "#222946", "#000000" }, // 06  6 black 0
  { "#001833", "#8C8B89", "#000000" }, // 07  7 black 8
  { "#001833", "#B32323", "#000000" }, // 08  8 red 1
  { "#001833", "#DC2C2C", "#000000" }, // 09  9 red 9      error
  { "#001833", "#1AAA13", "#000000" }, // 10  A green 2
  { "#001833", "#5DCE33", "#000000" }, // 11  B green 10   ok
  { "#001833", "#FF9F3E", "#000000" }, // 12  C yellow 3
  { "#001833", "#E6CB29", "#000000" }, // 13  D yellow 11  medium
  { "#001833", "#4957F3", "#000000" }, // 14  E blue 4
  { "#001833", "#4D79FF", "#000000" }, // 15  F blue 12
  { "#001833", "#BF3E94", "#000000" }, // 16 10 magenta 5
  { "#001833", "#B641CC", "#000000" }, // 17 11 magenta 13 hot
  { "#001833", "#09A5B3", "#000000" }, // 18 12 cyan 6     cool
  { "#001833", "#3CB6F3", "#000000" }, // 19 13 cyan 14
  { "#001833", "#AEAEAE", "#000000" }, // 20 14 white 7
};

static const unsigned int borderpx          = 1;    // border pixel of windows
static const unsigned int snap              = 32;   // snap pixel
static const unsigned int systrayspacing    = 2;    // systray spacing
static const Bool showsystray               = True; // False means no systray
static const Bool showbar                   = True; // False means no bar
static const Bool topbar                    = True; // False means bottom bar
static const unsigned int gappx             = 4;    // gap pixel between windows
static const Bool transbar                  = False;// True means transparent status bar/*}}}*/

/* Layout(s) *//*{{{*/

static const float mfact      = 0.50;   // factor of master area size [0.05..0.95]
static const int nmaster      = 1;      // number of clients in master area
static const Bool resizehints = False;  // True means respect size hints in tiled resizals

static const Layout layouts[] = {
    /* symbol   function */
    { "[þ]",    tile    },    // first entry is default
    { "[ý]",    NULL    },    // no layout means floating
    { "[ÿ]",    monocle },
    { "[ü]",    bstack  },
};/*}}}*/

/* Tagging *//*{{{*/

static const char *tags[] = { "1:email", "2:irc", "3:www", "4:media", "5:misc", 6 };

static const Rule rules[] = {
    /* class      instance          title       tags mask     isfloating   monitor */
    { "Gimp",           NULL,             NULL,       0,            True,        -1 },
    { "Thunar",         NULL,             NULL,       0,            True,        -1 },
    { "Steam",          NULL,             NULL,       0,            True,        -1 },
    { "Google-chrome",  NULL,             NULL,       1 << 2,       False,       -1 },
    { "Pidgin",         NULL,             NULL,       0,            True,        -1 },
    {  NULL,            "musicplayer",    NULL,       1 << 3,       False,       -1 },
    {  NULL,            "podcasts",       NULL,       1 << 3,       False,       -1 },
    {  NULL,            "twitter",        NULL,       1 << 1,       False,       -1 },
    {  NULL,            "weechat",        NULL,       1 << 1,       False,       -1 },
    {  NULL,            "email",          NULL,       1 << 0,       False,       -1 },
    {  NULL,            "mcabber",        NULL,       1 << 1,       False,       -1 },
    {  NULL,            "radio",          NULL,       1 << 3,       False,       -1 },
    {  NULL,            "news",           NULL,       1 << 3,       False,       -1 },

};/*}}}*/

/* key definitions *//*{{{*/
#define MODKEY Mod1Mask
#define TAGKEYS(KEY,TAG) \
    { MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
    { MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
    { MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },/*}}}*/

/* helper for spawning shell commands in the pre dwm-5.0 fashion *//*{{{*/
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }/*}}}*/

/* commands *//*{{{*/
static const char *dmenucmd[] =     { "dmenu_run", "-nb","#2c2e2f", "-nf", "#5e687f","-sb","#4d679a","-sf","#ffffff","-b", NULL };
static const char *screenshot[] =   { "/home/vasco/bin/scripts/screenshot", NULL };
static const char *sessioncmd[] =   { "/home/vasco/.dmenu/session", NULL };
static const char *gamescmd[] =     { "/home/vasco/.dmenu/games", NULL };
static const char *termcmd[]  =     { "urxvt", NULL };
static const char *thunar[] =       { "thunar", NULL };
static const char *upvol[] =        { "amixer", "set", "Master", "3+", NULL};
static const char *mutevol[] =      { "amixer", "set", "Master", "0%", NULL};
static const char *downvol[] =      { "amixer", "set", "Master", "3-", NULL};/*}}}*/

/* Keybindings *///*{{{*/

static Key keys[] = {
    // modifier                     key             function        argument
    { MODKEY|ShiftMask,             XK_Left,        movestack,      {.i = +1 } },
    { MODKEY|ShiftMask,             XK_Right,       movestack,      {.i = -1 } },
    { MODKEY|ShiftMask,             XK_Down,        movestack,      {.i = +1 } },
    { MODKEY|ShiftMask,             XK_Up,          movestack,      {.i = -1 } },
    { ControlMask,                  XK_Down,        moveresize,     {.v = (int []){ 0, 25, 0, 0 }}},
    { ControlMask,                  XK_Up,          moveresize,     {.v = (int []){ 0, -25, 0, 0 }}},
    { ControlMask,                  XK_Right,       moveresize,     {.v = (int []){ 25, 0, 0, 0 }}},
    { ControlMask,                  XK_Left,        moveresize,     {.v = (int []){ -25, 0, 0, 0 }}},
    { ControlMask|ShiftMask,        XK_Down,        moveresize,     {.v = (int []){ 0, 0, 0, 25 }}},
    { ControlMask|ShiftMask,        XK_Up,          moveresize,     {.v = (int []){ 0, 0, 0, -25 }}},
    { ControlMask|ShiftMask,        XK_Right,       moveresize,     {.v = (int []){ 0, 0, 25, 0 }}},
    { ControlMask|ShiftMask,        XK_Left,        moveresize,     {.v = (int []){ 0, 0, -25, 0 }}},
    { MODKEY,                       XK_minus,       spawn,          {.v = downvol } },
    { 0,                            XK_Print,       spawn,          {.v = screenshot}},
    { MODKEY,                       XK_plus,        spawn,          {.v = upvol } },
    { MODKEY,                       XK_0,           spawn,          {.v = mutevol } },
    { MODKEY,                       XK_r,           spawn,          {.v = dmenucmd } },
    { MODKEY,                       XK_F12,         spawn,          {.v = sessioncmd } },
    { MODKEY,                       XK_g,           spawn,          {.v = gamescmd } },
    { MODKEY,                       XK_Return,      spawn,          {.v = termcmd } },
    { MODKEY,                       XK_z,           spawn,          {.v = thunar } },
    { MODKEY|ShiftMask,             XK_r,           reload,         {0} },
    { MODKEY,                       XK_b,           togglebar,      {0} },
    { MODKEY,                       XK_j,           focusstack,     {.i = +1 } },
    { MODKEY,                       XK_k,           focusstack,     {.i = -1 } },
    { MODKEY,                       XK_Down,        focusstack,     {.i = +1 } },
    { MODKEY,                       XK_Up,          focusstack,     {.i = -1 } },
    { MODKEY,                       XK_v,           incnmaster,     {.i = +1 } },
    { MODKEY,                       XK_d,           incnmaster,     {.i = -1 } },
    { MODKEY,                       XK_h,           setmfact,       {.f = -0.05} },
    { MODKEY,                       XK_l,           setmfact,       {.f = +0.05} },
    { MODKEY,                       XK_Left,        setmfact,       {.f = -0.05} },
    { MODKEY,                       XK_Right,       setmfact,       {.f = +0.05} },
    { MODKEY,                       XK_Return,      zoom,           {0} },
    { MODKEY,                       XK_Tab,         view,           {0} },
    { MODKEY,                       XK_w,           killclient,     {0} },
    { MODKEY,                       XK_t,           setlayout,      {.v = &layouts[0]} },
    { MODKEY,                       XK_f,           setlayout,      {.v = &layouts[1]} },
    { MODKEY,                       XK_m,           setlayout,      {.v = &layouts[2]} },
    { MODKEY,                       XK_s,           setlayout,      {.v = &layouts[3]} },
    { MODKEY,                       XK_space,       setlayout,      {0} },
    { MODKEY|ShiftMask,             XK_space,       togglefloating, {0} },
    { MODKEY,                       XK_a,           view,           {.ui = ~0 } }, //view all tags at once
    { MODKEY|ShiftMask,             XK_0,           tag,            {.ui = ~0 } },
    { MODKEY,                       XK_comma,       focusmon,       {.i = -1 } },
    { MODKEY,                       XK_period,      focusmon,       {.i = +1 } },
    { MODKEY|ShiftMask,             XK_comma,       tagmon,         {.i = -1 } },
    { MODKEY|ShiftMask,             XK_period,      tagmon,         {.i = +1 } },
    TAGKEYS(                        XK_1,                           0)
    TAGKEYS(                        XK_2,                           1)
    TAGKEYS(                        XK_3,                           2)
    TAGKEYS(                        XK_4,                           3)
    TAGKEYS(                        XK_5,                           4)
    TAGKEYS(                        XK_6,                           5)
    TAGKEYS(                        XK_7,                           6)
    TAGKEYS(                        XK_8,                           7)
    TAGKEYS(                        XK_9,                           8)
    { MODKEY|ShiftMask,             XK_q,           quit,           {0} },
};/*}}}*/

#include "../tilemovemouse.c"

/* button definitions *//*{{{*/
/* click can be ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
    /* click                event mask      button          function        argument */
    { ClkLtSymbol,      0,               Button1,        setlayout,      {0} },
    { ClkLtSymbol,      0,               Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,      0,               Button2,        zoom,           {0} },
    { ClkStatusText,    0,               Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,     MODKEY,          Button1,        movemouse,      {0} },
    { ClkClientWin,     MODKEY,          Button2,        togglefloating, {0} },
    { ClkClientWin,     MODKEY,          Button3,        resizemouse,    {0} },
    { ClkTagBar,        0,               Button1,        view,           {0} },
    { ClkTagBar,        0,               Button3,        toggleview,     {0} },
    { ClkTagBar,        MODKEY,          Button1,        tag,            {0} },
    { ClkTagBar,        MODKEY,          Button3,        toggletag,      {0} },
    { ClkClientWin,     MODKEY|ShiftMask,Button1,        tilemovemouse,  {0} },
};
/*}}}*/
